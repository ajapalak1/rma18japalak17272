package com.example.pc.spirala1;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DohvatiKnjige extends AsyncTask<String, Integer, Void> {

    public interface IDohvatiKnjigeDone {
        public void onDohvatiKnjigeDone(ArrayList<Knjiga> rez);
    }

    ArrayList<Knjiga> knjigeDohvati;
    ArrayList<Knjiga> lista;
    private IDohvatiKnjigeDone pozivatelj;

    public DohvatiKnjige(IDohvatiKnjigeDone p) {
        pozivatelj = p;
        knjigeDohvati = new ArrayList<Knjiga>();
        lista = new ArrayList<Knjiga>();
    }

    @Override
    protected Void doInBackground(String... strings) {
        String query = null;
        String[] queries = null;
        try {
            query = URLEncoder.encode(strings[0], "utf-8");
            queries = query.split("%3B");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        for(int k = 0; k < queries.length; k++) {
            String url1 = "https://www.googleapis.com/books/v1/volumes?q=intitle:" + queries[k] + "&maxResults=5";
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                JSONArray books = jo.getJSONArray("items");
                for (int i = 0; i < books.length(); i++) {
                    JSONObject book = books.getJSONObject(i);
                    String id = book.getString("id");
                    JSONObject bookInfo = book.getJSONObject("volumeInfo");
                    String naziv = bookInfo.getString("title");
                        ArrayList<Autor> autori = new ArrayList<Autor>();
                        ArrayList<String> knjige = new ArrayList<String>();
                        knjige.add(id);
                        JSONArray imenaAutoraJson = new JSONArray();
                        try {
                            imenaAutoraJson = bookInfo.getJSONArray("authors");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for (int j = 0; j < imenaAutoraJson.length(); j++) {
                            autori.add(new Autor(imenaAutoraJson.getString(j), knjige));
                        }
                        if(autori.isEmpty()) autori.add(new Autor("Unkwown", knjige));
                        String opis = "Not given";
                        try {
                            opis = bookInfo.getString("description");
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                        String datumObjavljivanja = "";
                        try {
                            datumObjavljivanja= bookInfo.getString("publishedDate");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            datumObjavljivanja="2000";
                        }

                        JSONObject slike =  null;
                        try {
                           slike= bookInfo.getJSONObject("imageLinks");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        URL slika = null;
                        try {
                            if(slike == null) {
                               slika = new URL("http://rowwad.co/wp-content/uploads/2017/12/minimalist-design-wallpaper-elephant-minimalism-minimalist-graphic-design-wallpaper.jpg");
                            } else  {
                                slika = new URL(slike.getString("thumbnail"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        int brojStr = 0;
                        try{
                            brojStr = bookInfo.getInt("pageCount");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        knjigeDohvati.add(new Knjiga(id, naziv, autori, opis, datumObjavljivanja, slika, brojStr));
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void avoid) {
        super.onPostExecute(avoid);
        pozivatelj.onDohvatiKnjigeDone(knjigeDohvati);
    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {

        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
