package com.example.pc.spirala1;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC on 29.03.2018..
 */

public class KnjigaArrayAdapter extends ArrayAdapter<Knjiga> {
    int resource;
    public Resources res;
    public KnjigaArrayAdapter(@NonNull Context context, int _resource, List<Knjiga> items, Resources resLocal) {
        super(context, _resource, items);
        resource = _resource;
        res = resLocal;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent ){
        LinearLayout newView;
        if(convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }

        Knjiga classInstance = getItem(position);
        ImageView slika = (ImageView)newView.findViewById(R.id.eNaslovna);
        TextView nazivKnjige = (TextView)newView.findViewById(R.id.eNaziv);
        TextView imeAutora = (TextView)newView.findViewById(R.id.eAutor);
     /*   try {
            slika.setImageBitmap(BitmapFactory.decodeStream(getContext().openFileInput(classInstance.getNaziv())));
        } catch (FileNotFoundException e) {
            slika.setImageBitmap(null);
        }*/
        slika.setImageResource(R.drawable.slonic);
        nazivKnjige.setText(classInstance.getNaziv());
        String autori = "";
        ArrayList<Autor> autoriKnjige = new ArrayList<Autor>();
        autoriKnjige.addAll(classInstance.getAutori());
        for (int i = 0; i < autoriKnjige.size(); i++) {
            if(i == autoriKnjige.size() - 1)
                autori += autoriKnjige.get(i).getImeiPrezime();
            else
                autori += autoriKnjige.get(i).getImeiPrezime() + ", ";
        }
        imeAutora.setText(autori);
        return newView;
    }
}
