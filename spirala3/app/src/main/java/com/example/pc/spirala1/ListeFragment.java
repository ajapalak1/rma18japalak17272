package com.example.pc.spirala1;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC on 07.04.2018..
 */

public class ListeFragment extends Fragment {
    private ArrayList<Knjiga> knjige;
    private ListView lista;
    private ArrayList<String> unosi;
    private ArrayList<Autor> autoriLista;
    Context context;
    private ArrayAdapter<String> adapter;
    AutorArrayAdapter autorAdapter;
    onClick onButtondKnjigaClick;
    onListClick oic;
    Kategorije kat;
    FragmentManager fm;
    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
            context = getActivity();
            knjige = new ArrayList<Knjiga>();
            unosi = new ArrayList<String>();
            autoriLista = new ArrayList<Autor>();
            this.onViewStateRestored(savedInstance);
    }
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("knjige", knjige);
        outState.putParcelableArrayList("autori", autoriLista);
        outState.putStringArrayList("unosi", unosi);
    }
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            knjige = savedInstanceState.getParcelableArrayList("knjige");
            autoriLista = savedInstanceState.getParcelableArrayList("autori");
            unosi = savedInstanceState.getStringArrayList("unosi");
        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_liste, container, false);
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
        final Button pretraga = (Button)getActivity().findViewById(R.id.dPretraga);
        pretraga.setText(getActivity().getString(R.string.pretraga));
        final Button kategorija = (Button)getActivity().findViewById(R.id.dDodajKategoriju);
        kategorija.setText(getActivity().getString(R.string.kategorija));
        final Button knjiga = (Button)getActivity().findViewById(R.id.dDodajKnjigu);
        knjiga.setText(getActivity().getString(R.string.knjiga));
        final EditText tekst = (EditText)getActivity().findViewById(R.id.tekstPretraga);
        final Button autori = (Button)getActivity().findViewById(R.id.dAutori);
        autori.setText(getActivity().getString(R.string.autori));
        final Button dKategorije = (Button)getActivity().findViewById(R.id.dKategorije);
        dKategorije.setText(getActivity().getString(R.string.kategorije));
        final Resources res = getActivity().getResources();
        autorAdapter = new AutorArrayAdapter(context,R.layout.element_autor, autoriLista, res);
        kategorija.setEnabled(false);
        lista = (ListView)getActivity().findViewById(R.id.listaKategorija);
        adapter = new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1, unosi);
        lista.setAdapter(adapter);
        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(tekst.getText().toString())) {
                    adapter.getFilter().filter(tekst.getText().toString().toLowerCase(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int count) {
                            if(count == 0) {
                                kategorija.setEnabled(true);
                                kategorija.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        adapter = new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1, unosi);
                                        lista.setAdapter(adapter);
                                        unosi.add (tekst.getText().toString());
                                        adapter.notifyDataSetChanged();
                                        kat.Ikategorije(tekst.getText().toString());
                                        tekst.setText("");
                                        kategorija.setEnabled(false);

                                    }
                                });
                            }
                        }
                    });
                }
            }
        });

        knjiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             onButtondKnjigaClick.onButtondKnjigaClick(unosi);
            }
        });
        adapter.notifyDataSetChanged();
        autori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    autoriLista = getArguments().getParcelableArrayList("autori");
                } catch (Exception e) {

                }
                lista.setAdapter(autorAdapter);
                autorAdapter.notifyDataSetChanged();
                tekst.setVisibility(View.GONE);
                pretraga.setVisibility(View.GONE);
                kategorija.setVisibility(View.GONE);
            }
        });
        dKategorije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lista.setAdapter(adapter);
                tekst.setVisibility(View.VISIBLE);
                pretraga.setVisibility(View.VISIBLE);
                kategorija.setVisibility(View.VISIBLE);
            }
        });
        try {
            oic = (onListClick) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnListItemClick");
        }
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (lista.getAdapter().equals(adapter)) {
                    final ArrayList<Knjiga> poKategorijama = new ArrayList<Knjiga>();
                    String kategorija = unosi.get(i);
                    for (int j = 0; j < knjige.size(); j++) {
                        if (kategorija.equalsIgnoreCase(knjige.get(j).getKategorija()))
                            poKategorijama.add(knjige.get(j));
                    }
                    oic.onListItemClick(poKategorijama);
                } else {
                    final ArrayList<Knjiga> poAutorima = new ArrayList<Knjiga>();
                    Autor autor = autoriLista.get(i);
                    for(int j = 0; j < knjige.size(); j++) {
                        if(autor.getKnjige().contains(knjige.get(j).getId())) {
                            poAutorima.add(knjige.get(j));
                        }
                    }
                    oic.onListItemClick(poAutorima);
                }
            }
        });

    }

    public interface onClick {
        public void onButtondKnjigaClick(ArrayList<String> kategorije);
    }
    public interface Kategorije {
        public void Ikategorije(String kategorija);
    }
    public interface onListClick {
        public void onListItemClick(ArrayList<Knjiga> knjige);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onButtondKnjigaClick = (onClick) context;
            kat = (Kategorije) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onClick");
        }
    }

    public void setUIArguments(final Bundle args) {
       knjige = args.getParcelableArrayList("knjige");
       autoriLista = args.getParcelableArrayList("autori");
    }
}
