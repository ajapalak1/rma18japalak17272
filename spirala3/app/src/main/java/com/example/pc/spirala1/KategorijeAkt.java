package com.example.pc.spirala1;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class KategorijeAkt extends AppCompatActivity implements  ListeFragment.Kategorije,OnlineFragment.onBackClk, ListeFragment.onClick, DodavanjeKnjigeFragment.onClick, ListeFragment.onListClick, KnjigeFragment.onBackClick {
    private static FragmentManager fm;
    ArrayList<String> unosi;
    ArrayList<Knjiga> knjige;
    ArrayList<String> kat;
    ArrayList<Autor> autoriLista;
    DodavanjeKnjigeFragment dkf;
    ListeFragment lf;
    Boolean siriL;
    String FRAGMENT_TAG = "Tag";
    String FRAGMENT_TAG_DODAVANJE = "DodavanjeKnjige";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kategorije_akt);
        siriL = false;
        fm = getFragmentManager();
        FrameLayout lknjige = (FrameLayout)findViewById(R.id.mjestoF2);
        if(lknjige != null) {
            siriL = true;
            KnjigeFragment knjigeFragment;
            knjigeFragment = (KnjigeFragment)fm.findFragmentById(R.id.mjestoF2);
            if(knjigeFragment == null) {
                knjigeFragment = new KnjigeFragment();
                fm.beginTransaction().replace(R.id.mjestoF2, knjigeFragment).commit();
            }
        }
        if(fm.findFragmentById(R.id.mjestoF1) != null)  {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            lf = (ListeFragment) fm.findFragmentById(R.id.mjestoF1);
            if (lf == null) {
                lf = new ListeFragment();
                fm.beginTransaction().replace(R.id.mjestoF1, lf, FRAGMENT_TAG).commit();
            } else {
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
        knjige = new ArrayList<Knjiga>();
        autoriLista = new ArrayList<Autor>();
        kat = new ArrayList<String>();
        final Button dDodajOnline = (Button)findViewById(R.id.dDodajOnline);
        dDodajOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                OnlineFragment onlineFragment = new OnlineFragment();
                arguments.putStringArrayList("kategorije", kat);
                onlineFragment.setArguments(arguments);
                if(siriL) {
                    getFragmentManager().beginTransaction().replace(R.id.mjestoF2, onlineFragment).commit();
                } else {
                    fm.beginTransaction().replace(R.id.mjestoF1, onlineFragment).addToBackStack(null).commit();
                }
            }
        });
    }
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("knjige", knjige);
        outState.putParcelableArrayList("autori", autoriLista);
        outState.putStringArrayList("unosi", unosi);
    }
    @Override
    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            knjige = savedInstanceState.getParcelableArrayList("knjige");
            autoriLista = savedInstanceState.getParcelableArrayList("autori");
            unosi = savedInstanceState.getStringArrayList("unosi");
        }

    }

    @Override
    public void onButtondKnjigaClick(ArrayList<String> kategorije) {
        Bundle argumenti = new Bundle();
        if(siriL) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, FrameLayout.LayoutParams.MATCH_PARENT);
            params.weight = 2;
            FrameLayout flay = (FrameLayout)findViewById(R.id.mjestoF1);
            flay.setLayoutParams(params);
            argumenti.putBoolean("siriL", true);
        } else {
            argumenti.putBoolean("siriL", false);
        }
        if (dkf == null) {
            dkf = new DodavanjeKnjigeFragment();
        }
        argumenti.putStringArrayList("kategorije", kategorije);
        argumenti.putParcelableArrayList("autori", autoriLista);
        dkf.setArguments(argumenti);
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.mjestoF1, dkf, FRAGMENT_TAG_DODAVANJE).addToBackStack(null).commit();
    }

    @Override
    public void onListItemClick(ArrayList<Knjiga> _knjige) {
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList("knjige",_knjige);
        KnjigeFragment knjigeFragment = new KnjigeFragment();
        knjigeFragment.setArguments(arguments);
        if(siriL) {
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, knjigeFragment).commit();
        } else {
            fm.beginTransaction().replace(R.id.mjestoF1, knjigeFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onClick(ArrayList<Knjiga> _knjige, ArrayList<Autor> _autoriLista, Boolean _siriL) {
        lf = (ListeFragment) fm.findFragmentByTag(FRAGMENT_TAG);
        if(lf == null) {
            lf = new ListeFragment();
        }
        if(!_knjige.isEmpty()) {
            knjige.addAll(_knjige);
            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("knjige", knjige);
            argumenti.putParcelableArrayList("autori", autoriLista);
            lf.setUIArguments(argumenti);
        }

        if(siriL) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, FrameLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1;
            FrameLayout flay = (FrameLayout)findViewById(R.id.mjestoF1);
            flay.setLayoutParams(params);
        }
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onBackButton() {
        fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
    @Override
    public void onBackPressed() {
        dkf = (DodavanjeKnjigeFragment) fm.findFragmentByTag(FRAGMENT_TAG_DODAVANJE);

    }

    @Override
    public void onBackButtonClk(ArrayList<Knjiga> _knjige) {
        for(int i = 0; i < _knjige.size();i++) {
            Boolean vecPostoji = false;
            for(int j = 0; j < knjige.size();j++) {
                if(knjige.get(j).getId().equals(_knjige.get(i).getId())) {
                    vecPostoji = true;
                    break;
                }
            }
            if(!vecPostoji) knjige.add(_knjige.get(i));
        }
        for(int i = 0; i < _knjige.size(); i++) {
            for(int j = 0; j < _knjige.get(i).getAutori().size(); j++) {
                Boolean vecPostoji = false;
                for(int k = 0; k < autoriLista.size(); k++) {
                    if(autoriLista.get(k).getImeiPrezime().equals(_knjige.get(i).getAutori().get(j).getImeiPrezime())) {
                        autoriLista.get(k).dodajKnjigu(_knjige.get(i).getId());
                        vecPostoji = true;
                        break;
                    }
                }
                if(!vecPostoji) autoriLista.add(_knjige.get(i).getAutori().get(j));
            }
        }
        lf = (ListeFragment) fm.findFragmentByTag(FRAGMENT_TAG);
        if(lf == null) {
            lf = new ListeFragment();
        }
        Bundle argumenti = new Bundle();
        argumenti.putParcelableArrayList("knjige", knjige);
        argumenti.putParcelableArrayList("autori", autoriLista);
        lf.setUIArguments(argumenti);

        fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void Ikategorije(String kategorija) {
        kat.add(kategorija);
    }


}
