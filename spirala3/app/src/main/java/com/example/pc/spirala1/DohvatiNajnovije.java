package com.example.pc.spirala1;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class DohvatiNajnovije extends AsyncTask<String, Integer, Void> {
    public interface IDohvatiNajnovijeDone {
        public void onNajnovijeDone(ArrayList<Knjiga> rez);
    }

    ArrayList<Knjiga> knjigeDohvati;
    ArrayList<Knjiga> lista;
    private DohvatiNajnovije.IDohvatiNajnovijeDone pozivatelj;

    public DohvatiNajnovije(DohvatiNajnovije.IDohvatiNajnovijeDone p) {
        pozivatelj = p;
        knjigeDohvati = new ArrayList<Knjiga>();
        lista = new ArrayList<Knjiga>();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected Void doInBackground(String... strings) {
        String queryDohvati = null;
        String query = null;

        try {
            queryDohvati = URLEncoder.encode(strings[0], "utf-8");
            query = queryDohvati.substring(8);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
            String url1 = "https://www.googleapis.com/books/v1/volumes?q=inauthor:" + query;
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String rezultat = convertStreamToString(in);
                JSONObject jo = new JSONObject(rezultat);
                JSONArray books = jo.getJSONArray("items");
                ArrayList<JSONObject> booksFromAuthor = new ArrayList<JSONObject>();
                for (int i = 0; i < books.length(); i++) {
                    booksFromAuthor.add(books.getJSONObject(i));
                }
                for (int i = 0 ; i < booksFromAuthor.size(); i++) {
                    Collections.sort(booksFromAuthor, new Comparator<JSONObject>() {
                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public int compare(JSONObject o1, JSONObject o2) {
                            String date1 = "";
                            String date2 = "";
                            JSONObject info1 = null;
                            JSONObject info2 = null;
                            try {
                                info1 = o1.getJSONObject("volumeInfo");
                                info2 = o2.getJSONObject("volumeInfo");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                date1 = info1.getString("publishedDate");
                                date2 = info2.getString("publishedDate");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if(date1.length() <= 5 || date2.length() <= 5) {
                                return -Integer.compare(Integer.parseInt(date1.substring(0, 4)), (Integer.parseInt(date2.substring(0, 4))));
                            }
                            if(date1.substring(0,4).equals(date2.substring(0,4))) {
                                if(date1.substring(5,7).equals(date2.substring(5,7))) {
                                    return -Integer.compare(Integer.parseInt(date1.substring(8)), (Integer.parseInt(date2.substring(8))));
                                } else {
                                    return -Integer.compare(Integer.parseInt(date1.substring(5, 7)), (Integer.parseInt(date2.substring(5, 7))));
                                }

                            }
                            return -Integer.compare(Integer.parseInt(date1.substring(0, 4)), (Integer.parseInt(date2.substring(0, 4))));
                        }
                    });
                }
                for (int i = 0; i < booksFromAuthor.size(); i++) {
                    if(i == 5) break;
                    JSONObject book = booksFromAuthor.get(i);
                    String id = book.getString("id");
                    JSONObject bookInfo = book.getJSONObject("volumeInfo");
                    String naziv = bookInfo.getString("title");
                        ArrayList<Autor> autori = new ArrayList<Autor>();
                        ArrayList<String> knjige = new ArrayList<String>();
                        knjige.add(id);
                        JSONArray imenaAutoraJson = new JSONArray();
                        try {
                            imenaAutoraJson = bookInfo.getJSONArray("authors");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for (int j = 0; j < imenaAutoraJson.length(); j++) {
                            autori.add(new Autor(imenaAutoraJson.getString(j), knjige));
                        }
                        if (autori.isEmpty()) autori.add(new Autor("Unkown", knjige));
                        String opis = "Not given";
                        try {
                            opis = bookInfo.getString("description");
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                        String datumObjavljivanja = bookInfo.getString("publishedDate");
                        JSONObject slike =  null;
                        try {
                            slike = bookInfo.getJSONObject("imageLinks");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        URL slika;
                        if(slike == null) {
                            slika = new URL("http://rowwad.co/wp-content/uploads/2017/12/minimalist-design-wallpaper-elephant-minimalism-minimalist-graphic-design-wallpaper.jpg");
                        } else  {
                            slika = new URL(slike.getString("thumbnail"));
                        }

                        int brojStr = 0;
                        try{
                            brojStr = bookInfo.getInt("pageCount");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        knjigeDohvati.add(new Knjiga(id, naziv, autori, opis, datumObjavljivanja, slika, brojStr));
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        return null;
    }

    @Override
    protected void onPostExecute(Void avoid) {
        super.onPostExecute(avoid);
        pozivatelj.onNajnovijeDone(knjigeDohvati);
    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {

        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
