package com.example.pc.spirala1;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class MyReciever extends ResultReceiver {
    private Receiver receiver;
    /**
     * Create a new ResultReceive to receive results.  Your
     * {@link #onReceiveResult} method will be called from the thread running
     * <var>handler</var> if given, or from an arbitrary thread if null.
     *
     * @param handler
     */
    public MyReciever(Handler handler) {
        super(handler);
    }
    public void setReceiver(Receiver mreceiver) {
        this.receiver = mreceiver;
    }

    public interface Receiver {
        public void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if(receiver != null) {
            receiver.onReceiveResult(resultCode,resultData);
        }
    }
}
