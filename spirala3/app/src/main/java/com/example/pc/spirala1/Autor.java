package com.example.pc.spirala1;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by PC on 08.04.2018..
 */

public class Autor implements Parcelable {
    String imeiPrezime;
    ArrayList<String> knjige;
    public Autor() {}
    public Autor(String ime, ArrayList<String> knjige) {
        this.imeiPrezime = ime;
        this.knjige = new ArrayList<String>();
        this.knjige = knjige;
    }

    public Autor(Parcel parcel) {
        this.imeiPrezime = parcel.readString();
        this.knjige = parcel.readArrayList(String.class.getClassLoader());
    }
    public void setImeiPrezimee(String ime) {
        this.imeiPrezime = ime;
    }

    public String getImeiPrezime() {
        return imeiPrezime;
    }

    public void setKnjige(ArrayList<String> knjige) {
        this.knjige = knjige;
    }

    public ArrayList<String> getKnjige() {
        return knjige;
    }

    public void dodajKnjigu(String id) {
        if(!knjige.contains(id))
            knjige.add(id);
    }
    public int getBrojKnjiga() {
        return knjige.size();
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(imeiPrezime);
        parcel.writeStringList(knjige);
    }
    public static Parcelable.Creator<Autor> CREATOR = new Creator<Autor>() {
        @Override
        public Autor createFromParcel(Parcel parcel) {
            return new Autor(parcel);
        }

        @Override
        public Autor[] newArray(int i) {
            return new Autor[i];
        }
    };
}
