package com.example.pc.spirala1;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by PC on 07.04.2018..
 */

public class DodavanjeKnjigeFragment extends Fragment {
    ImageView slika;
    EditText imeAutora;
    EditText nazivKnjige;
    ArrayList<Autor> autoriLista;
    onClick onClick;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_dodavanje_knjige, container, false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
        final Boolean siriL = getArguments().getBoolean("siriL");
        autoriLista = new ArrayList<Autor>();
        autoriLista = getArguments().getParcelableArrayList("autori");
        slika = (ImageView)getActivity().findViewById(R.id.naslovnaStr);
        imeAutora = (EditText)getActivity().findViewById(R.id.imeAutora);
        nazivKnjige = (EditText)getActivity().findViewById(R.id.nazivKnjige);
        final Spinner kategorije = (Spinner)getActivity().findViewById(R.id.sKategorijaKnjige);
        Button nadjiSliku = (Button)getActivity().findViewById(R.id.dNadjiSliku);
        Button dodajAutora = (Button)getActivity().findViewById(R.id.dDodajAutora);
        nadjiSliku.setText(getActivity().getString(R.string.slika));
        final EditText id = (EditText)getActivity().findViewById(R.id.id);
        Button dodajKnjigu = (Button)getActivity().findViewById(R.id.dUpisiKnjigu);
        dodajKnjigu.setText(getActivity().getString(R.string.dodajKnjigu));
        Button ponisti = (Button)getActivity().findViewById(R.id.dPonisti);
        ponisti.setText(getActivity().getString(R.string.ponisti));
        final ArrayList<Knjiga> knjige = new ArrayList<Knjiga>();
        ArrayList<String> kategorijeZaSpinner = new ArrayList<String>();
        kategorijeZaSpinner = getArguments().getStringArrayList("kategorije");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kategorijeZaSpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kategorije.setAdapter(adapter);
        final ArrayList<Autor> autoriKnjige = new ArrayList<Autor>();
        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Knjiga knjiga = new Knjiga();
                if(kategorije.getCount() == 0 || TextUtils.isEmpty(nazivKnjige.getText())) {
                    Toast.makeText(getActivity() ,"Popunite sva polja", Toast.LENGTH_LONG).show();
                } else {
                    if (kategorije.getSelectedItem().toString().isEmpty())
                        if (kategorije.getChildCount() == 0)
                            knjiga.setKategorija("");
                        else
                            knjiga.setKategorija(kategorije.getItemAtPosition(0).toString());
                    else
                        knjiga.setKategorija(kategorije.getSelectedItem().toString());
                    knjiga.setNaziv(nazivKnjige.getText().toString());
                    knjiga.setAutori(autoriKnjige);
                    knjige.add(knjiga);
                    imeAutora.setText("");
                    nazivKnjige.setText("");
                    autoriKnjige.clear();
                    id.setText("");
                    slika.setImageBitmap(null);
                }
            }
        });
        dodajAutora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!autoriLista.isEmpty()) {
                    for (int i = 0; i < autoriLista.size(); i++) {
                        if (imeAutora.getText().toString().equals(autoriLista.get(i).imeiPrezime)) {
                            autoriLista.get(i).dodajKnjigu(id.getText().toString());
                            autoriKnjige.add(new Autor(autoriLista.get(i).getImeiPrezime(), autoriLista.get(i).getKnjige()));
                            break;
                        } else {
                            ArrayList<String> knj = new ArrayList<String>();
                            knj.add(id.getText().toString());
                            autoriLista.add(new Autor(imeAutora.getText().toString(), knj));
                            autoriKnjige.add(new Autor(imeAutora.getText().toString(), knj));
                            break;
                        }
                    }
                } else {
                    ArrayList<String> knj = new ArrayList<String>();
                    knj.add(id.getText().toString());
                    autoriLista.add(new Autor(imeAutora.getText().toString(), knj));
                    autoriKnjige.add(new Autor(imeAutora.getText().toString(), knj));
                }
                imeAutora.setText("");
            }
        });
        ponisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onClick(knjige,autoriLista, siriL);
            }
        });

        nadjiSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Choose Picture"), 1);
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode==RESULT_CANCELED)
        {
            // action cancelled
        }
        if(resultCode==RESULT_OK)
        {
            try {
                FileOutputStream outputStream;
                outputStream = getActivity().openFileOutput(nazivKnjige.getText().toString(), Context.MODE_PRIVATE);
                getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG,90,outputStream);
                outputStream.close();
                slika.setImageBitmap(BitmapFactory.decodeStream(getActivity().openFileInput(nazivKnjige.getText().toString())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getActivity().getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }
    public interface onClick {
        public void onClick(ArrayList<Knjiga> knjige, ArrayList<Autor> autoriLista, Boolean siriL);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onClick = (onClick) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onClick");
        }
    }
}
