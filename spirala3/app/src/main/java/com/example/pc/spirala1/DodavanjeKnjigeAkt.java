package com.example.pc.spirala1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class DodavanjeKnjigeAkt extends AppCompatActivity {
    ImageView slika;
    EditText imeAutora;
    EditText nazivKnjige;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodavanje_knjige_akt);
        slika = (ImageView)findViewById(R.id.naslovnaStr);
        imeAutora = (EditText)findViewById(R.id.imeAutora);
        nazivKnjige = (EditText)findViewById(R.id.nazivKnjige);
        final Spinner kategorije = (Spinner)findViewById(R.id.sKategorijaKnjige);
        Button nadjiSliku = (Button)findViewById(R.id.dNadjiSliku);
        Button dodajKnjigu = (Button)findViewById(R.id.dUpisiKnjigu);
        Button ponisti = (Button)findViewById(R.id.dPonisti);
        final ArrayList<Knjiga> knjige = new ArrayList<Knjiga>();
        ArrayList<String> kategorijeZaSpinner = new ArrayList<String>();
        kategorijeZaSpinner = getIntent().getStringArrayListExtra("kategorije");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, kategorijeZaSpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kategorije.setAdapter(adapter);
        dodajKnjigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Knjiga knjiga = new Knjiga();
                if(kategorije.getCount() == 0 || TextUtils.isEmpty(nazivKnjige.getText()) || TextUtils.isEmpty(imeAutora.getText())) {
                    Toast.makeText(DodavanjeKnjigeAkt.this ,"Popunite sva polja", Toast.LENGTH_LONG).show();
                } else {
                    if (kategorije.getSelectedItem().toString().isEmpty())
                        if (kategorije.getChildCount() == 0)
                            knjiga.setKategorija("");
                        else
                            knjiga.setKategorija(kategorije.getItemAtPosition(0).toString());
                    else
                        knjiga.setKategorija(kategorije.getSelectedItem().toString());
                    knjiga.setNaziv(nazivKnjige.getText().toString());
                  //  knjiga.setImeAutora(imeAutora.getText().toString());
                    knjige.add(knjiga);
                    imeAutora.setText("");
                    nazivKnjige.setText("");
                    slika.setImageBitmap(null);
                }
            }
        });
        ponisti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DodavanjeKnjigeAkt.this, KategorijeAkt.class);
                if(!knjige.isEmpty()) {
                    intent.putParcelableArrayListExtra("knjige", knjige);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    setResult(Activity.RESULT_CANCELED, intent);
                    finish();
                }
            }
        });
        nadjiSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Choose Picture"), 1);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode==RESULT_CANCELED)
        {
            // action cancelled
        }
        if(resultCode==RESULT_OK)
        {
            try {
                FileOutputStream outputStream;
                outputStream = openFileOutput(nazivKnjige.getText().toString(), Context.MODE_PRIVATE);
                getBitmapFromUri(data.getData()).compress(Bitmap.CompressFormat.JPEG,90,outputStream);
                outputStream.close();
                slika.setImageBitmap(BitmapFactory.decodeStream(openFileInput(nazivKnjige.getText().toString())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }
}
