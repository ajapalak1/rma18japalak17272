package com.example.pc.spirala1;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by PC on 08.04.2018..
 */

public class AutorArrayAdapter extends ArrayAdapter<Autor> {
    private final int resource;
    private final Resources res;

    public AutorArrayAdapter(@NonNull Context context, int _resource, List<Autor> items, Resources resLocal) {
        super(context, _resource, items);
        resource = _resource;
        res = resLocal;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent ){
        LinearLayout newView;
        if(convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }

        Autor classInstance = getItem(position);
        TextView ime = (TextView) newView.findViewById(R.id.eIme);
        TextView brojKnjiga = (TextView)newView.findViewById(R.id.eBrojKnjiga);

        ime.setText(classInstance.getImeiPrezime());
        brojKnjiga.setText(String.valueOf(classInstance.getBrojKnjiga()));
        return newView;
    }
}
