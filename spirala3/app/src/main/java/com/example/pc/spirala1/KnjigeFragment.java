package com.example.pc.spirala1;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by PC on 08.04.2018..
 */

public class KnjigeFragment extends Fragment {
    ArrayList<Knjiga> knjige;
    KnjigaArrayAdapter adapter;
    private onBackClick onBackButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        View vi = inflater.inflate(R.layout.fragment_knjige, container, false);
        knjige = new ArrayList<Knjiga>();

        if(getArguments() != null && getArguments().containsKey("knjige")) {
            knjige = getArguments().getParcelableArrayList("knjige");
            final ListView lista = (ListView)vi.findViewById(R.id.listaKnjiga);
            Button povratak = (Button)vi.findViewById(R.id.dPovratak);
            povratak.setText(getActivity().getString(R.string.povratak));
            Resources res = getActivity().getResources();
            adapter = new KnjigaArrayAdapter(getActivity(), R.layout.element_liste, knjige, res);
            lista.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    lista.getChildAt(i).setBackgroundColor(getActivity().getColor(R.color.colorElementPressed));
                }
            });
            povratak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackButton.onBackButton();
                }
            });
        }
        return vi;
    }
    public interface onBackClick {
        public void onBackButton();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onBackButton = (onBackClick) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onClick");
        }
    }
}
