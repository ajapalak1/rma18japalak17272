package com.example.pc.spirala1;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class ListaKnjigaAkt extends AppCompatActivity {
    ArrayList<Knjiga> knjige;
    KnjigaArrayAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_knjiga_akt);
        knjige = getIntent().getParcelableArrayListExtra("knjige");
        final ListView lista = (ListView)findViewById(R.id.listaKnjiga);
        Button povratak = (Button)findViewById(R.id.dPovratak);
        Resources res = getResources();
        adapter = new KnjigaArrayAdapter(this, R.layout.element_liste, knjige, res);
        lista.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lista.getChildAt(i).setBackgroundColor(0xffaabbed);
            }
        });
        povratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListaKnjigaAkt.this, KategorijeAkt.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivityIfNeeded(intent,0);
            }
        });
    }
}
