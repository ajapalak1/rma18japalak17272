package com.example.pc.spirala1;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC on 29.03.2018..
 */

public class KnjigaArrayAdapter extends ArrayAdapter<Knjiga> {
    onButtonClick onClick;
    int resource;
    public Resources res;
    public KnjigaArrayAdapter(@NonNull Context context, int _resource, List<Knjiga> items, Resources resLocal) {
        super(context, _resource, items);
        resource = _resource;
        res = resLocal;

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent ){
        LinearLayout newView;
        if(convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }
        onClick = (onButtonClick) getContext();
        final Knjiga classInstance = getItem(position);
        ImageView slika = (ImageView)newView.findViewById(R.id.eNaslovna);
        TextView nazivKnjige = (TextView)newView.findViewById(R.id.eNaziv);
        TextView imeAutora = (TextView)newView.findViewById(R.id.eAutor);
        TextView datumObjavljivanja = (TextView)newView.findViewById(R.id.eDatumObjavljivanja);
        TextView opis = (TextView)newView.findViewById(R.id.eOpis);
        TextView brojStranica = (TextView)newView.findViewById(R.id.eBrojStranica);
        Button preporuci = (Button)newView.findViewById(R.id.dPreporuci);
   /*     try {
            slika.setImageBitmap(BitmapFactory.decodeStream(getContext().openFileInput(classInstance.getNaziv())));
        } catch (FileNotFoundException e) {
            slika.setImageResource(R.drawable.slonic);
        }*/
        nazivKnjige.setText(classInstance.getNaziv());
        datumObjavljivanja.setText(classInstance.getDatumObjavljivanja());
        opis.setText(classInstance.getOpis());
        int stranice = classInstance.getBrojStranica();
        String straniceString = Integer.toString(stranice);
        brojStranica.setText(straniceString);
        String url = classInstance.getSlika().toString();
        Picasso.get().load(url).into(slika);
        String autori = "";
        ArrayList<Autor> autoriKnjige = new ArrayList<Autor>();
        autoriKnjige.addAll(classInstance.getAutori());
        for (int i = 0; i < autoriKnjige.size(); i++) {
            if(i == autoriKnjige.size() - 1)
                autori += autoriKnjige.get(i).getImeiPrezime();
            else
                autori += autoriKnjige.get(i).getImeiPrezime() + ", ";
        }
        imeAutora.setText(autori);
        final int pozicija = position;
        preporuci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onClick(position);
            }
        });
        return newView;
    }
    public interface onButtonClick{
        public void onClick(int position);
    }
}
