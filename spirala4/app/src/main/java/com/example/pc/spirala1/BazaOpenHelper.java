package com.example.pc.spirala1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BazaOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "baza.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_KATEGORIJA = "Kategorija";
    public static final String TABLE_KNJIGA = "Knjiga";
    public static final String TABLE_AUTOR = "Autor";
    public static final String TABLE_AUTORSTVO = "Autorstvo";
    public static final String ID = "_id";
    //TABELA KATEGORIJA
    public static final String KATEGORIJA_NAZIV = "naziv";
    public static final String DATABASE_CREATE_KATEGORIJA = "create table " + TABLE_KATEGORIJA +
    "("  + ID + " integer primary key autoincrement, "
            + KATEGORIJA_NAZIV + " text not null);";
    //TABELA KNJIGA
    public static final String KNJIGA_NAZIV = "naziv";
    public static final String KNJIGA_OPIS = "opis";
    public static final String KNJIGA_DATUM_OBJAVLJIVANJA = "datumObjavljivanja";
    public static final String KNJIGA_BROJ_STRANICA = "brojStranica";
    public static final String KNJIGA_ID_WEB_SERVIS = "idWebServis";
    public static final String KNJIGA_ID_KATEGORIJE = "idkategorije";
    public static final String DATABASE_CREATE_KNJIGA = "create table " + TABLE_KNJIGA +
            "(" + ID + " integer primary key autoincrement, "
            + KNJIGA_NAZIV + " text not null, " + KNJIGA_OPIS + " text not null, " +
            KNJIGA_DATUM_OBJAVLJIVANJA + " text not null, " +
            KNJIGA_BROJ_STRANICA + " integer not null, " +
            KNJIGA_ID_WEB_SERVIS + " text not null, " +
            KNJIGA_ID_KATEGORIJE + " integer not null);";
    //TABELA AUTOR
    public static final String AUTOR_IME = "ime";
    public static final String DATABASE_CREATE_AUTOR = "create table " + TABLE_AUTOR +
            "(" + ID + " integer primary key autoincrement, "
            + AUTOR_IME + " text not null);";
    //TABELA AUTORSTVO
    public static final String AUTORSTVO_ID_AUTORA = "idAutora";
    public static final String AUTORSTVO_ID_KNJIGE = "idKnjige";
    public static final String DATABASE_CREATE_AUTORSTVO = "create table " + TABLE_AUTORSTVO +
            "("  + ID + " integer primary key autoincrement, "
            + AUTORSTVO_ID_AUTORA  + " integer not null, " +
            AUTORSTVO_ID_KNJIGE + " integer not null);";


    public BazaOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public BazaOpenHelper(Context context) {
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_KATEGORIJA);
        db.execSQL(DATABASE_CREATE_KNJIGA);
        db.execSQL(DATABASE_CREATE_AUTOR);
        db.execSQL(DATABASE_CREATE_AUTORSTVO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_KATEGORIJA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTOR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_KNJIGA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTORSTVO);
        onCreate(db);
    }
}
