package com.example.pc.spirala1;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;

public class KategorijeAkt extends AppCompatActivity implements  ListeFragment.Kategorije,OnlineFragment.onBackClk, ListeFragment.onClick, DodavanjeKnjigeFragment.onClick, ListeFragment.onListClick, KnjigeFragment.onBackClick, KnjigaArrayAdapter.onButtonClick {
    private static FragmentManager fm;
    ArrayList<String> unosi;
    ArrayList<Knjiga> knjige;
    ArrayList<String> kat;
    ArrayList<Autor> autoriLista;
    DodavanjeKnjigeFragment dkf;
    ListeFragment lf;
    Boolean siriL;
    String FRAGMENT_TAG = "Tag";
    String FRAGMENT_TAG_DODAVANJE = "DodavanjeKnjige";
    public static final String ID = "_id";
    //TABELA KATEGORIJA
    public static final String KATEGORIJA_NAZIV = "naziv";
    //TABELA KNJIGA
    public static final String KNJIGA_NAZIV = "naziv";
    public static final String KNJIGA_OPIS = "opis";
    public static final String KNJIGA_DATUM_OBJAVLJIVANJA = "datumObjavljivanja";
    public static final String KNJIGA_BROJ_STRANICA = "brojStranica";
    public static final String KNJIGA_ID_WEB_SERVIS = "idWebServis";
    public static final String KNJIGA_ID_KATEGORIJE = "idkategorije";
    public static final String AUTOR_IME = "ime";
    public static final String AUTORSTVO_ID_AUTORA = "idAutora";
    public static final String AUTORSTVO_ID_KNJIGE = "idKnjige";


    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.activity_kategorije_akt);
        kat = new ArrayList<String>();
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(this);
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        String [] koloneRezultat = {ID, KATEGORIJA_NAZIV};
        Cursor cursor = db.query(bazaOpenHelper.TABLE_KATEGORIJA, koloneRezultat, null, null, null, null,null);
        int INDEX_KOLONE;
        try {
            INDEX_KOLONE =  cursor.getColumnIndexOrThrow(KATEGORIJA_NAZIV);
        } catch (Exception e) {
            Log.d("Ime: ", "onActivityCreated: " + e);
            INDEX_KOLONE = Integer.parseInt(null);
        }
        while (cursor.moveToNext()) {
            if(!kat.contains(cursor.getString(INDEX_KOLONE)))
                kat.add(cursor.getString(INDEX_KOLONE));
        }
        siriL = false;
        fm = getFragmentManager();
        FrameLayout lknjige = (FrameLayout)findViewById(R.id.mjestoF2);
        if(lknjige != null) {
            siriL = true;
            KnjigeFragment knjigeFragment;
            knjigeFragment = (KnjigeFragment)fm.findFragmentById(R.id.mjestoF2);
            if(knjigeFragment == null) {
                knjigeFragment = new KnjigeFragment();
                fm.beginTransaction().replace(R.id.mjestoF2, knjigeFragment).commit();
            }
        }
        if(fm.findFragmentById(R.id.mjestoF1) != null)  {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            lf = (ListeFragment) fm.findFragmentById(R.id.mjestoF1);
            if (lf == null) {
                lf = new ListeFragment();
                fm.beginTransaction().replace(R.id.mjestoF1, lf, FRAGMENT_TAG).commit();
            } else {
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
        knjige = new ArrayList<Knjiga>();
        autoriLista = new ArrayList<Autor>();
        final Button dDodajOnline = (Button)findViewById(R.id.dDodajOnline);
        dDodajOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                OnlineFragment onlineFragment = new OnlineFragment();
                arguments.putStringArrayList("kategorije", kat);
                onlineFragment.setArguments(arguments);
                if(siriL) {
                    getFragmentManager().beginTransaction().replace(R.id.mjestoF2, onlineFragment).commit();
                } else {
                    fm.beginTransaction().replace(R.id.mjestoF1, onlineFragment).addToBackStack(null).commit();
                }
            }
        });
    }

    long dodajKnjigu(Knjiga knjiga) {
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(this);
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        String[] koloneRezultat = {KNJIGA_ID_WEB_SERVIS, ID};
        boolean postoji = false;
        int idKategorije = -1;
        Cursor cursor = db.query(bazaOpenHelper.TABLE_KNJIGA, koloneRezultat, null, null, null, null, null);
        int INDEX_KOLONE, INDEX_KOLONE_ID;
        try {
            INDEX_KOLONE =  cursor.getColumnIndexOrThrow(KNJIGA_ID_WEB_SERVIS);
        } catch (Exception e) {
            Log.d("Ime: ", "onActivityCreated: " + e);
            INDEX_KOLONE = Integer.parseInt(null);
        }
        while (cursor.moveToNext()) {
            if(cursor.getString(INDEX_KOLONE).equals(knjiga.getId())) {
                postoji = true;
            }
        }
        if(!postoji) {
            bazaOpenHelper = new BazaOpenHelper(this);
            db = bazaOpenHelper.getWritableDatabase();
            koloneRezultat = new String[]{ID, KATEGORIJA_NAZIV};
            cursor = db.query(bazaOpenHelper.TABLE_KATEGORIJA, koloneRezultat, null, null, null, null, null);
            try {
                INDEX_KOLONE =  cursor.getColumnIndexOrThrow(KATEGORIJA_NAZIV);
                INDEX_KOLONE_ID = cursor.getColumnIndexOrThrow(ID);
            } catch (Exception e) {
                Log.d("Ime: ", "onActivityCreated: " + e);
                INDEX_KOLONE = Integer.parseInt(null);
                INDEX_KOLONE_ID = Integer.parseInt(null);
            }
            while (cursor.moveToNext()) {
                if(cursor.getString(INDEX_KOLONE).equals(knjiga.getKategorija()))
                    idKategorije = cursor.getInt(INDEX_KOLONE_ID);
            }
            ContentValues novi = new ContentValues();
            novi.put(KNJIGA_NAZIV, knjiga.getNaziv());
            novi.put(KNJIGA_OPIS, knjiga.getOpis());
            novi.put(KNJIGA_BROJ_STRANICA, knjiga.getBrojStranica());
            novi.put(KNJIGA_DATUM_OBJAVLJIVANJA, knjiga.getDatumObjavljivanja());
            novi.put(KNJIGA_ID_WEB_SERVIS, knjiga.getId());
            novi.put(KNJIGA_ID_KATEGORIJE, idKategorije);
            bazaOpenHelper = new BazaOpenHelper(this);
            db = bazaOpenHelper.getWritableDatabase();
            db.insert(bazaOpenHelper.TABLE_KNJIGA, null, novi);
            koloneRezultat = new String[]{KNJIGA_ID_WEB_SERVIS, ID};
            cursor = db.query(bazaOpenHelper.TABLE_KNJIGA, koloneRezultat, null, null, null, null, null);
            try {
                INDEX_KOLONE = cursor.getColumnIndexOrThrow(KNJIGA_ID_WEB_SERVIS);
                INDEX_KOLONE_ID = cursor.getColumnIndexOrThrow(ID);
            } catch (Exception e) {
                Log.d("Ime: ", "onActivityCreated: " + e);
                INDEX_KOLONE = Integer.parseInt(null);
                INDEX_KOLONE_ID = Integer.parseInt(null);
            }
            while (cursor.moveToLast()) {
                if (cursor.getString(INDEX_KOLONE).equals(knjiga.getId())) {
                    dodajUAutore(knjiga);
                    return cursor.getInt(INDEX_KOLONE_ID);
                }
            }
        }
        return -1;
    }

    public void dodajUAutore(Knjiga knjiga) {
        for(int i = 0; i < knjiga.getAutori().size(); i++) {
            BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(this);
            SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
            String[] koloneRezultat = {AUTOR_IME, ID};
            boolean postoji = false;
            Cursor cursor = db.query(bazaOpenHelper.TABLE_AUTOR, koloneRezultat, null, null, null, null, null);
            int INDEX_KOLONE;
            try {
                INDEX_KOLONE = cursor.getColumnIndexOrThrow(AUTOR_IME);
            } catch (Exception e) {
                Log.d("Ime: ", "onActivityCreated: " + e);
                INDEX_KOLONE = Integer.parseInt(null);
            }
            while (cursor.moveToNext()) {
                if (cursor.getString(INDEX_KOLONE).equals(knjiga.getAutori().get(i).getImeiPrezime())) {
                    postoji = true;
                }
            }
            if(!postoji) {
                ContentValues novi = new ContentValues();
                novi.put(AUTOR_IME, knjiga.getAutori().get(i).getImeiPrezime());
                bazaOpenHelper = new BazaOpenHelper(this);
                db = bazaOpenHelper.getWritableDatabase();
                db.insert(bazaOpenHelper.TABLE_AUTOR, null, novi);
                dodajUAutorstvo(knjiga, i);
            }
        }
    }

    private void dodajUAutorstvo(Knjiga knjiga, int i) {
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(this);
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        int idKnjige = -1, idAutora = -1;
        String[] koloneRezultat = new String[]{KNJIGA_ID_WEB_SERVIS, ID};
        Cursor cursor = db.query(bazaOpenHelper.TABLE_KNJIGA, koloneRezultat, null, null, null, null, null);
        int INDEX_KOLONE, INDEX_KOLONE_ID;
        try {
            INDEX_KOLONE = cursor.getColumnIndexOrThrow(KNJIGA_ID_WEB_SERVIS);
            INDEX_KOLONE_ID = cursor.getColumnIndexOrThrow(ID);
        } catch (Exception e) {
            Log.d("Ime: ", "onActivityCreated: " + e);
            INDEX_KOLONE = Integer.parseInt(null);
            INDEX_KOLONE_ID = Integer.parseInt(null);
        }
        while (cursor.moveToNext()) {
            if (cursor.getString(INDEX_KOLONE).equals(knjiga.getId())) {
                idKnjige = cursor.getInt(INDEX_KOLONE_ID);
            }
        }
        koloneRezultat = new String[]{AUTOR_IME, ID};
        cursor = db.query(bazaOpenHelper.TABLE_AUTOR, koloneRezultat, null, null, null, null, null);
        try {
            INDEX_KOLONE = cursor.getColumnIndexOrThrow(AUTOR_IME);
            INDEX_KOLONE_ID = cursor.getColumnIndexOrThrow(ID);
        } catch (Exception e) {
            Log.d("Ime: ", "onActivityCreated: " + e);
            INDEX_KOLONE = Integer.parseInt(null);
            INDEX_KOLONE_ID = Integer.parseInt(null);
        }
        while (cursor.moveToNext()) {
            if (cursor.getString(INDEX_KOLONE).equals(knjiga.getAutori().get(i).getImeiPrezime())) {
                idAutora = cursor.getInt(INDEX_KOLONE_ID);
            }
        }
        if(idAutora != -1 && idKnjige != -1) {
            ContentValues novi = new ContentValues();
            novi.put(AUTORSTVO_ID_AUTORA, idAutora);
            novi.put(AUTORSTVO_ID_KNJIGE, idKnjige);
            bazaOpenHelper = new BazaOpenHelper(this);
            db = bazaOpenHelper.getWritableDatabase();
            db.insert(bazaOpenHelper.TABLE_AUTORSTVO, null, novi);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("knjige", knjige);
        outState.putParcelableArrayList("autori", autoriLista);
        outState.putStringArrayList("unosi", unosi);
    }
    @Override
    public void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            knjige = savedInstanceState.getParcelableArrayList("knjige");
            autoriLista = savedInstanceState.getParcelableArrayList("autori");
            unosi = savedInstanceState.getStringArrayList("unosi");
        }

    }

    @Override
    public void onButtondKnjigaClick(ArrayList<String> kategorije) {
        Bundle argumenti = new Bundle();
        if(siriL) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, FrameLayout.LayoutParams.MATCH_PARENT);
            params.weight = 2;
            FrameLayout flay = (FrameLayout)findViewById(R.id.mjestoF1);
            flay.setLayoutParams(params);
            argumenti.putBoolean("siriL", true);
        } else {
            argumenti.putBoolean("siriL", false);
        }
        if (dkf == null) {
            dkf = new DodavanjeKnjigeFragment();
        }
        argumenti.putStringArrayList("kategorije", kategorije);
        argumenti.putParcelableArrayList("autori", autoriLista);
        dkf.setArguments(argumenti);
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.mjestoF1, dkf, FRAGMENT_TAG_DODAVANJE).addToBackStack(null).commit();
    }

    @Override
    public void onListItemClick(ArrayList<Knjiga> _knjige) {
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList("knjige",_knjige);
        KnjigeFragment knjigeFragment = new KnjigeFragment();
        knjigeFragment.setArguments(arguments);
        if(siriL) {
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, knjigeFragment).commit();
        } else {
            fm.beginTransaction().replace(R.id.mjestoF1, knjigeFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onClick(ArrayList<Knjiga> _knjige, ArrayList<Autor> _autoriLista, Boolean _siriL) {
        lf = (ListeFragment) fm.findFragmentByTag(FRAGMENT_TAG);
        if(lf == null) {
            lf = new ListeFragment();
        }
        if(!_knjige.isEmpty()) {
            knjige.addAll(_knjige);
            Bundle argumenti = new Bundle();
            argumenti.putParcelableArrayList("knjige", knjige);
            argumenti.putParcelableArrayList("autori", autoriLista);
            lf.setUIArguments(argumenti);
        }

        if(siriL) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, FrameLayout.LayoutParams.MATCH_PARENT);
            params.weight = 1;
            FrameLayout flay = (FrameLayout)findViewById(R.id.mjestoF1);
            flay.setLayoutParams(params);
        }
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onBackButton() {
        fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
    @Override
    public void onBackPressed() {
        dkf = (DodavanjeKnjigeFragment) fm.findFragmentByTag(FRAGMENT_TAG_DODAVANJE);

    }

    @Override
    public void onBackButtonClk(ArrayList<Knjiga> _knjige) {
        for(int i = 0; i < _knjige.size();i++) {
            Boolean vecPostoji = false;
            for(int j = 0; j < knjige.size();j++) {
                if(knjige.get(j).getId().equals(_knjige.get(i).getId())) {
                    vecPostoji = true;
                    break;
                }
            }
            if(!vecPostoji) knjige.add(_knjige.get(i));
            dodajKnjigu(_knjige.get(i));
        }
        for(int i = 0; i < _knjige.size(); i++) {
            for(int j = 0; j < _knjige.get(i).getAutori().size(); j++) {
                Boolean vecPostoji = false;
                for(int k = 0; k < autoriLista.size(); k++) {
                    if(autoriLista.get(k).getImeiPrezime().equals(_knjige.get(i).getAutori().get(j).getImeiPrezime())) {
                        autoriLista.get(k).dodajKnjigu(_knjige.get(i).getId());
                        vecPostoji = true;
                        break;
                    }
                }
                if(!vecPostoji) autoriLista.add(_knjige.get(i).getAutori().get(j));
            }
        }
        lf = (ListeFragment) fm.findFragmentByTag(FRAGMENT_TAG);
        if(lf == null) {
            lf = new ListeFragment();
        }
        Bundle argumenti = new Bundle();
        argumenti.putParcelableArrayList("knjige", knjige);
        argumenti.putParcelableArrayList("autori", autoriLista);
        lf.setUIArguments(argumenti);

        fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void Ikategorije(String kategorija) {
        kat.add(kategorija);
    }


    @Override
    public void onClick(int position) {
        FragmentPreporuci preporuka = new FragmentPreporuci();
        Bundle bundle = new Bundle();
        bundle.putParcelable("knjiga", knjige.get(position));
        preporuka.setArguments(bundle);
        if(siriL) {
            getFragmentManager().beginTransaction().replace(R.id.mjestoF2, preporuka).commit();
        } else {
            fm.beginTransaction().replace(R.id.mjestoF1, preporuka).addToBackStack(null).commit();
        }
    }
}
