package com.example.pc.spirala1;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PC on 07.04.2018..
 */

public class ListeFragment extends Fragment {
    private ArrayList<Knjiga> knjige;
    private ListView lista;
    private ArrayList<String> unosi;
    private ArrayList<Autor> autoriLista;
    Context context;
    private ArrayAdapter<String> adapter;
    AutorArrayAdapter autorAdapter;
    onClick onButtondKnjigaClick;
    onListClick oic;
    Kategorije kat;
    FragmentManager fm;
    public static final String ID = "_id";
    //TABELA KATEGORIJA
    public static final String KATEGORIJA_NAZIV = "naziv";
    //TABELA KNJIGA
    public static final String KNJIGA_NAZIV = "naziv";
    public static final String KNJIGA_OPIS = "opis";
    public static final String KNJIGA_DATUM_OBJAVLJIVANJA = "datumObjavljivanja";
    public static final String KNJIGA_BROJ_STRANICA = "brojStranica";
    public static final String KNJIGA_ID_WEB_SERVIS = "idWebServis";
    public static final String KNJIGA_ID_KATEGORIJE = "idkategorije";
    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
            context = getActivity();
            knjige = new ArrayList<Knjiga>();
            unosi = new ArrayList<String>();
            autoriLista = new ArrayList<Autor>();
            this.onViewStateRestored(savedInstance);
    }
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("knjige", knjige);
        outState.putParcelableArrayList("autori", autoriLista);
        outState.putStringArrayList("unosi", unosi);
    }
    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            knjige = savedInstanceState.getParcelableArrayList("knjige");
            autoriLista = savedInstanceState.getParcelableArrayList("autori");
            unosi = savedInstanceState.getStringArrayList("unosi");
        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        return inflater.inflate(R.layout.fragment_liste, container, false);
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onActivityCreated(Bundle savedInstance) {
        super.onActivityCreated(savedInstance);
        unosiUpdate();
        final Button pretraga = (Button)getActivity().findViewById(R.id.dPretraga);
        pretraga.setText(getActivity().getString(R.string.pretraga));
        final Button kategorija = (Button)getActivity().findViewById(R.id.dDodajKategoriju);
        kategorija.setText(getActivity().getString(R.string.kategorija));
        final Button knjiga = (Button)getActivity().findViewById(R.id.dDodajKnjigu);
        knjiga.setText(getActivity().getString(R.string.knjiga));
        final EditText tekst = (EditText)getActivity().findViewById(R.id.tekstPretraga);
        final Button autori = (Button)getActivity().findViewById(R.id.dAutori);
        autori.setText(getActivity().getString(R.string.autori));
        final Button dKategorije = (Button)getActivity().findViewById(R.id.dKategorije);
        dKategorije.setText(getActivity().getString(R.string.kategorije));
        final Resources res = getActivity().getResources();
        autorAdapter = new AutorArrayAdapter(context,R.layout.element_autor, autoriLista, res);
        kategorija.setEnabled(false);
        lista = (ListView)getActivity().findViewById(R.id.listaKategorija);
        adapter = new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1, unosi);
        lista.setAdapter(adapter);
        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(tekst.getText().toString())) {
                    adapter.getFilter().filter(tekst.getText().toString().toLowerCase(), new Filter.FilterListener() {
                        @Override
                        public void onFilterComplete(int count) {
                            if(count == 0) {
                                kategorija.setEnabled(true);
                                kategorija.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        adapter = new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1, unosi);
                                        lista.setAdapter(adapter);
                                        int poruka = (int) dodajKategoriju(tekst.getText().toString());
                                        String sPoruka = Integer.toString(poruka);
                                        Log.d("Dodano", sPoruka);
                                        adapter.notifyDataSetChanged();
                                        kat.Ikategorije(tekst.getText().toString());
                                        tekst.setText("");
                                        kategorija.setEnabled(false);

                                    }
                                });
                            }
                        }
                    });
                }
            }
        });


        knjiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             onButtondKnjigaClick.onButtondKnjigaClick(unosi);
            }
        });
        adapter.notifyDataSetChanged();
        autori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    autoriLista = getArguments().getParcelableArrayList("autori");
                } catch (Exception e) {

                }
                lista.setAdapter(autorAdapter);
                autorAdapter.notifyDataSetChanged();
                tekst.setVisibility(View.GONE);
                pretraga.setVisibility(View.GONE);
                kategorija.setVisibility(View.GONE);
            }
        });
        dKategorije.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lista.setAdapter(adapter);
                tekst.setVisibility(View.VISIBLE);
                pretraga.setVisibility(View.VISIBLE);
                kategorija.setVisibility(View.VISIBLE);
            }
        });
        try {
            oic = (onListClick) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + "Treba implementirati OnListItemClick");
        }
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (lista.getAdapter().equals(adapter)) {
                    final ArrayList<Knjiga> poKategorijama = new ArrayList<Knjiga>();
                    String kategorija = unosi.get(i);
                    for (int j = 0; j < knjige.size(); j++) {
                        if (kategorija.equalsIgnoreCase(knjige.get(j).getKategorija()))
                            poKategorijama.add(knjige.get(j));
                    }
                    oic.onListItemClick(poKategorijama);
                } else {
                    final ArrayList<Knjiga> poAutorima = new ArrayList<Knjiga>();
                    Autor autor = autoriLista.get(i);
                    for(int j = 0; j < knjige.size(); j++) {
                        if(autor.getKnjige().contains(knjige.get(j).getId())) {
                            poAutorima.add(knjige.get(j));
                        }
                    }
                    oic.onListItemClick(poAutorima);
                }
            }
        });

    }

  /*  private ArrayList<Knjiga> knjigeKategorije(int indeks) {
        ArrayList<Knjiga> knjige = new ArrayList<Knjiga>();
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        db = bazaOpenHelper.getWritableDatabase();
        String[] koloneRezultat = new String[]{KNJIGA_NAZIV, KNJIGA_OPIS, KNJIGA_BROJ_STRANICA, KNJIGA_DATUM_OBJAVLJIVANJA, KNJIGA_ID_WEB_SERVIS, KNJIGA_ID_KATEGORIJE};
        Cursor cursor = db.query(bazaOpenHelper.TABLE_KNJIGA, koloneRezultat, null, null, null, null, null);
        int INDEX_KOLONE, INDEX_NAZIV = 0, INDEX_OPIS, INDEX_BR_STRANICA, INDEX_DATUM_OBJAVLJIVANJA,INDEX_KATEGORIJE, INDEX_URL;
        try {
            INDEX_NAZIV = cursor.getColumnIndexOrThrow(KNJIGA_NAZIV);
            INDEX_OPIS = cursor.getColumnIndexOrThrow(KNJIGA_OPIS);
            INDEX_BR_STRANICA = cursor.getColumnIndexOrThrow(KNJIGA_BROJ_STRANICA);
            INDEX_DATUM_OBJAVLJIVANJA = cursor.getColumnIndexOrThrow(KNJIGA_DATUM_OBJAVLJIVANJA);
            INDEX_KOLONE = cursor.getColumnIndexOrThrow(KNJIGA_ID_WEB_SERVIS);
            INDEX_KATEGORIJE = cursor.getColumnIndexOrThrow(KNJIGA_ID_KATEGORIJE);

        } catch (Exception e) {
            Log.d("Ime: ", "onActivityCreated: " + e);
            INDEX_KOLONE = Integer.parseInt(null);
            INDEX_KATEGORIJE = Integer.parseInt(null);
        }
        while (cursor.moveToNext()) {
            if (cursor.getInt(INDEX_KATEGORIJE) == indeks) {
                String kategorija = nadjiKategoriju(indeks);
                ArrayList<String> autori = nadjiAutore(cursor.getString(INDEX_KOLONE));
                knjige.add(new Knjiga(cursor.getString(INDEX_KOLONE),cursor.getString(INDEX_NAZIV),kategorija, ));
            }
        }
        return knjige;
    }

    private ArrayList<String> nadjiAutore(String string) {
    }

    private String nadjiKategoriju(int indeks) {
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        String [] koloneRezultat = {ID, KATEGORIJA_NAZIV};
        Cursor cursor = db.query(bazaOpenHelper.TABLE_KATEGORIJA, koloneRezultat, null, null, null, null,null);
        int INDEX_KOLONE, NAZIV = 0;
        try {
            INDEX_KOLONE =  cursor.getColumnIndexOrThrow(ID);
            NAZIV = cursor.getColumnIndexOrThrow(KATEGORIJA_NAZIV);
        } catch (Exception e) {
            Log.d("Ime: ", "onActivityCreated: " + e);
            INDEX_KOLONE = Integer.parseInt(null);
        }
        while (cursor.moveToNext()) {
            if(cursor.getInt(INDEX_KOLONE) == indeks)
                return cursor.getString(NAZIV);
        }
        return "";
    }*/

    long dodajKategoriju(String naziv) {
        ContentValues novi = new ContentValues();
        novi.put(KATEGORIJA_NAZIV, naziv);
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        db.insert(bazaOpenHelper.TABLE_KATEGORIJA, null, novi);
        unosiUpdate();
        String[] koloneRezultat = {ID, KATEGORIJA_NAZIV};
        Cursor cursor = db.query(bazaOpenHelper.TABLE_KATEGORIJA, koloneRezultat, null, null, null, null, null);
        int INDEX_KOLONE, INDEX_KOLONE_ID;
        try {
            INDEX_KOLONE =  cursor.getColumnIndexOrThrow(KATEGORIJA_NAZIV);
            INDEX_KOLONE_ID = cursor.getColumnIndexOrThrow(ID);
        } catch (Exception e) {
            Log.d("Ime: ", "onActivityCreated: " + e);
            INDEX_KOLONE = Integer.parseInt(null);
            INDEX_KOLONE_ID = Integer.parseInt(null);
        }
        while (cursor.moveToLast()) {
            if(cursor.getString(INDEX_KOLONE).equals(naziv))
                return cursor.getInt(INDEX_KOLONE_ID);
        }
        return -1;
    }
    public void unosiUpdate() {
        BazaOpenHelper bazaOpenHelper = new BazaOpenHelper(getActivity());
        SQLiteDatabase db = bazaOpenHelper.getWritableDatabase();
        String [] koloneRezultat = {ID, KATEGORIJA_NAZIV};
        Cursor cursor = db.query(bazaOpenHelper.TABLE_KATEGORIJA, koloneRezultat, null, null, null, null,null);
        int INDEX_KOLONE;
        try {
            INDEX_KOLONE =  cursor.getColumnIndexOrThrow(KATEGORIJA_NAZIV);
        } catch (Exception e) {
            Log.d("Ime: ", "onActivityCreated: " + e);
            INDEX_KOLONE = Integer.parseInt(null);
        }
        while (cursor.moveToNext()) {
            if(!unosi.contains(cursor.getString(INDEX_KOLONE)))
                unosi.add(cursor.getString(INDEX_KOLONE));
        }
    }

    public interface onClick {
        public void onButtondKnjigaClick(ArrayList<String> kategorije);
    }
    public interface Kategorije {
        public void Ikategorije(String kategorija);
    }
    public interface onListClick {
        public void onListItemClick(ArrayList<Knjiga> knjige);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onButtondKnjigaClick = (onClick) context;
            kat = (Kategorije) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onClick");
        }
    }

    public void setUIArguments(final Bundle args) {
       knjige = args.getParcelableArrayList("knjige");
       autoriLista = args.getParcelableArrayList("autori");
    }
}
