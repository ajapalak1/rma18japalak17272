package com.example.pc.spirala1;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class KnjigePoznanika extends IntentService {
    public static final int STATUS_START = 0;
    public static final int STATUS_FINISH= 1;
    public static final int STATUS_ERROR= 2;


    public KnjigePoznanika() {
        super(null);
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public KnjigePoznanika(String name) {
        super(name);
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        final ResultReceiver resultReceiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();
        resultReceiver.send(STATUS_START, Bundle.EMPTY);
        String idKorisnika = intent.getStringExtra("idKorisnika").substring(9);
        ArrayList<Knjiga> listaKnjiga = new ArrayList<Knjiga>();
        String url1 = "https://www.googleapis.com/books/v1/users/" + idKorisnika + "/bookshelves";
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);
            JSONObject jo = new JSONObject(rezultat);
            JSONArray shelfs = jo.getJSONArray("items");
            for(int i = 0; i < shelfs.length(); i++) {
                JSONObject shelf = shelfs.getJSONObject(i);
                String url2 = "https://www.googleapis.com/books/v1/users/" + idKorisnika + "/bookshelves/" + shelf.getString("id") + "/volumes";
                url = new URL(url2);
                urlConnection = (HttpURLConnection) url.openConnection();
                in = new BufferedInputStream(urlConnection.getInputStream());
                String shelfRez = convertStreamToString(in);
                jo = new JSONObject(shelfRez);
                JSONArray books = jo.getJSONArray("items");
                for (int k = 0; k < books.length(); k++) {
                    JSONObject book = books.getJSONObject(k);
                    String id = book.getString("id");
                    JSONObject bookInfo = book.getJSONObject("volumeInfo");
                    String naziv = bookInfo.getString("title");
                    ArrayList<Autor> autori = new ArrayList<Autor>();
                    ArrayList<String> knjige = new ArrayList<String>();
                    knjige.add(id);
                    JSONArray imenaAutoraJson = new JSONArray();
                    try {
                        imenaAutoraJson = bookInfo.getJSONArray("authors");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    for (int j = 0; j < imenaAutoraJson.length(); j++) {
                        autori.add(new Autor(imenaAutoraJson.getString(j), knjige));
                    }
                    String opis = "Not given";
                    try {
                        opis = bookInfo.getString("description");
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    String datumObjavljivanja = "";
                    try {
                        datumObjavljivanja= bookInfo.getString("publishedDate");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JSONObject slike =  null;
                    try {
                        slike= bookInfo.getJSONObject("imageLinks");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    URL slika = null;
                    try {
                        if(slike == null) {
                            slika = new URL("http://rowwad.co/wp-content/uploads/2017/12/minimalist-design-wallpaper-elephant-minimalism-minimalist-graphic-design-wallpaper.jpg");
                        } else  {
                            slika = new URL(slike.getString("thumbnail"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    int brojStr = 0;
                    try{
                        brojStr = bookInfo.getInt("pageCount");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    listaKnjiga.add(new Knjiga(id, naziv, autori, opis, datumObjavljivanja, slika, brojStr));
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
            resultReceiver.send(STATUS_ERROR, Bundle.EMPTY);
        } catch (JSONException e) {
            e.printStackTrace();
            bundle.putString(Intent.EXTRA_TEXT,e.toString());
            resultReceiver.send(STATUS_ERROR, bundle);
        }
        bundle.putParcelableArrayList("knjige", listaKnjiga);
        resultReceiver.send(STATUS_FINISH, bundle);
    }

        @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {

        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


}

