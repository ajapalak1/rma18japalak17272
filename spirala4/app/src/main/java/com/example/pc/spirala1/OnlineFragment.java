package com.example.pc.spirala1;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class OnlineFragment extends Fragment implements DohvatiKnjige.IDohvatiKnjigeDone, DohvatiNajnovije.IDohvatiNajnovijeDone, MyReciever.Receiver{
    ArrayList<Knjiga> knjige;
    Spinner sRezultati;
    Spinner sKategorije;
    private onBackClk onBackButton;
    ArrayList<String> spinKnjige;
    ArrayAdapter<String> adapter;
    ArrayList<Knjiga> savedBooks;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstance) {
        View vi = inflater.inflate(R.layout.fragment_online, container, false);
        knjige = new ArrayList<Knjiga>();
        savedBooks = new ArrayList<Knjiga>();
        spinKnjige = new ArrayList<String>();
        final Button drun = (Button)vi.findViewById(R.id.dRun);
        final EditText tekstUpit = (EditText)vi.findViewById(R.id.tekstUpit);
        sRezultati = (Spinner)vi.findViewById(R.id.sRezultat);
        sKategorije = (Spinner)vi.findViewById(R.id.sKategorije);
        ArrayList<String> kategorije = getArguments().getStringArrayList("kategorije");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, kategorije);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sKategorije.setAdapter(adapter1);
        adapter1.notifyDataSetChanged();
        drun.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if(tekstUpit.getText().toString().startsWith("autor:")) {
                    new DohvatiNajnovije((DohvatiNajnovije.IDohvatiNajnovijeDone)OnlineFragment.this).execute(tekstUpit.getText().toString());
                } else  if(tekstUpit.getText().toString().startsWith("korisnik:")) {
                   Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), KnjigePoznanika.class);
                    MyReciever receiver = new MyReciever(new Handler());
                    receiver.setReceiver(OnlineFragment.this);
                    intent.putExtra("idKorisnika", tekstUpit.getText().toString());
                    intent.putExtra("receiver", receiver);
                    getActivity().startService(intent);
                }else {
                    if (!tekstUpit.getText().toString().isEmpty() && !tekstUpit.getText().toString().contains(" "))
                        new DohvatiKnjige((DohvatiKnjige.IDohvatiKnjigeDone) OnlineFragment.this).execute(tekstUpit.getText().toString());
                    else {
                        Toast.makeText(getContext(), "Niste unijeli ispravne parametre", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        final Button dPovratak = (Button)vi.findViewById(R.id.dPovratak1);
        dPovratak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackButton.onBackButtonClk(savedBooks);
            }
        });

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, spinKnjige);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sRezultati.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        final Button dAdd = (Button)vi.findViewById(R.id.dAdd);
        dAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sKategorije.getSelectedItem() != null) {
                    if (!knjige.isEmpty()) {
                        int indeks = sRezultati.getSelectedItemPosition();
                        savedBooks.add(knjige.get(indeks));
                        knjige.get(indeks).setKategorija(sKategorije.getSelectedItem().toString());
                    }
                }
            }
        });
        return vi;
    }

    @Override
    public void onDohvatiKnjigeDone(ArrayList<Knjiga> rez) {
        knjige.clear();
        knjige.addAll(rez);
        spinKnjige.clear();
        for (int i = 0; i < knjige.size(); i++) {
               spinKnjige.add(knjige.get(i).getNaziv());
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onNajnovijeDone(ArrayList<Knjiga> rez) {
        knjige.clear();
        knjige.addAll(rez);
        spinKnjige.clear();
        for (int i = 0; i < knjige.size(); i++) {
            spinKnjige.add(knjige.get(i).getNaziv());
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case KnjigePoznanika.STATUS_START:
                break;
            case KnjigePoznanika.STATUS_FINISH:
                knjige.clear();
                knjige.addAll(resultData.<Knjiga>getParcelableArrayList("knjige"));
                spinKnjige.clear();
                for (int i = 0; i < knjige.size(); i++) {
                    spinKnjige.add(knjige.get(i).getNaziv());
                }
                adapter.notifyDataSetChanged();
                break;
            case KnjigePoznanika.STATUS_ERROR:
                String error = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(getActivity(),error,Toast.LENGTH_LONG).show();
                break;
        }
    }

    public interface onBackClk {
        public void onBackButtonClk(ArrayList<Knjiga> knjige);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onBackButton = (onBackClk) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onClick");
        }
    }
}
