package com.example.pc.spirala1;

public class Contact {
    String ime;
    String email;

    public Contact() {}
    public Contact(String ime, String email) {
        this.ime = ime;
        this.email = email;
    }

    public String getIme() {
        return ime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }
}
