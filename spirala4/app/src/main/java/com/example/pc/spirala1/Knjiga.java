package com.example.pc.spirala1;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.widget.ImageView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by PC on 27.03.2018..
 */

public class Knjiga implements Parcelable{
    String id;
    String naziv;
    ArrayList<Autor> autori;
    String opis;
    String datumObjavljivanja;
    URL slika;
    int brojStranica;
    String kategorija;

    public Knjiga() {
        this.autori = new ArrayList<Autor>();
    }
    public Knjiga(String id, String naziv, ArrayList<Autor> autori, String opis, String datumObjavljivanja, URL slika,int brojStranica) {
        this.id = id;
        this.naziv = naziv;
        this.autori = new ArrayList<Autor>();
        this.autori.addAll(autori);
        this.opis = opis;
        this.datumObjavljivanja = datumObjavljivanja;
        this.slika = slika;
        this.brojStranica = brojStranica;
    }
    public Knjiga(ArrayList<Autor> autori, String naziv, String kategorija) {
        this.autori.addAll(autori);
        this.naziv = naziv;
        this.kategorija = kategorija;
    }
    public Knjiga(Parcel parcel){
        this.id = parcel.readString();
        this.naziv = parcel.readString();
        this.autori = parcel.readArrayList(Autor.class.getClassLoader());
        this.opis = parcel.readString();
        this.datumObjavljivanja = parcel.readString();
        this.slika = (URL) parcel.readSerializable();
        this.brojStranica = parcel.readInt();
        this.kategorija = parcel.readString();
    }

    public void setAutori(ArrayList<Autor> autori) { this.autori.addAll(autori);    }

    public ArrayList<Autor> getAutori() { return autori; }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setNaziv(String nazivKnjige) {
        this.naziv = nazivKnjige;
    }

    public String getNaziv() {
        return naziv;
    }

    public String getId() {
        return id;
    }

    public int getBrojStranica() {
        return brojStranica;
    }

    public String getDatumObjavljivanja() {
        return datumObjavljivanja;
    }

    public String getOpis() {
        return opis;
    }

    public URL getSlika() {
        return slika;
    }

    public void setBrojStranica(int brojStranica) {
        this.brojStranica = brojStranica;
    }

    public void setDatumObjavljivanja(String datumObjavljivanja) {
        this.datumObjavljivanja = datumObjavljivanja;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void setSlika(URL slika) {
        this.slika = slika;
    }
    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable((Parcelable)this.autori, i);
        parcel.writeString(this.id);
        parcel.writeString(this.naziv);
        parcel.writeString(this.opis);
        parcel.writeString(this.datumObjavljivanja);
        parcel.writeSerializable(this.slika);
        parcel.writeInt(this.brojStranica);
        parcel.writeString(this.kategorija);
    }
    public static Parcelable.Creator<Knjiga> CREATOR = new Parcelable.Creator<Knjiga>() {
        @Override
        public Knjiga createFromParcel(Parcel parcel) {
            return new Knjiga(parcel);
        }

        @Override
        public Knjiga[] newArray(int i) {
            return new Knjiga[i];
        }
    };
}
