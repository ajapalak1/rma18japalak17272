package com.example.pc.spirala1;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.provider.ContactsContract;
import com.squareup.picasso.Picasso;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.widget.Toast;

import java.time.Instant;
import java.util.ArrayList;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;


public class FragmentPreporuci extends Fragment {
    Knjiga classInstance;
    ArrayList<String> allContactsEmails;
    ArrayList<Contact> allContacts;
    private ContentResolver contResv;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        View vi = inflater.inflate(R.layout.fragment_preporuci, container, false);

        if(getArguments() != null && getArguments().containsKey("knjiga")) {
            contResv = getContext().getContentResolver();
            allContactsEmails = new ArrayList<String>();
            allContacts = new ArrayList<Contact>();
            getContacts();
            final Spinner kontakti = (Spinner)vi.findViewById(R.id.sKontakti);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),  android.R.layout.simple_spinner_item, allContactsEmails);
            kontakti.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            classInstance = getArguments().getParcelable("knjiga");
            ImageView slika = (ImageView)vi.findViewById(R.id.eNaslovna);
            TextView nazivKnjige = (TextView)vi.findViewById(R.id.eNaziv);
            TextView imeAutora = (TextView)vi.findViewById(R.id.eAutor);
            TextView datumObjavljivanja = (TextView)vi.findViewById(R.id.eDatumObjavljivanja);
            TextView opis = (TextView)vi.findViewById(R.id.eOpis);
            TextView brojStranica = (TextView)vi.findViewById(R.id.eBrojStranica);
            Button posalji = (Button)vi.findViewById(R.id.dPosalji);
            nazivKnjige.setText(classInstance.getNaziv());
            datumObjavljivanja.setText(classInstance.getDatumObjavljivanja());
            opis.setText(classInstance.getOpis());
            int stranice = classInstance.getBrojStranica();
            String straniceString = Integer.toString(stranice);
            brojStranica.setText(straniceString);
            String url = classInstance.getSlika().toString();
            Picasso.get().load(url).into(slika);
            String autori = "";
            ArrayList<Autor> autoriKnjige = new ArrayList<Autor>();
            autoriKnjige.addAll(classInstance.getAutori());
            for (int i = 0; i < autoriKnjige.size(); i++) {
                if(i == autoriKnjige.size() - 1)
                    autori += autoriKnjige.get(i).getImeiPrezime();
                else
                    autori += autoriKnjige.get(i).getImeiPrezime() + ", ";
            }
            imeAutora.setText(autori);
            final String finalAutori = autori;
            posalji.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(kontakti.getSelectedItem() != null) {
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");
                        String TO = kontakti.getSelectedItem().toString();
                        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{TO});
                        String ime = "";
                        for (int i = 0; i < allContacts.size(); i++) {
                            if(allContacts.get(i).getEmail().equals(TO)) {
                                ime = allContacts.get(i).getIme();
                                break;
                            }
                        }
                        String text = "Zdravo " + ime + "," + System.lineSeparator() + "Pročitaj knjigu " + classInstance.getNaziv() + " od " + finalAutori + "!";
                        emailIntent.putExtra(Intent.EXTRA_TEXT, text);
                        try {
                            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                            getActivity().finish();
                        } catch (android.content.ActivityNotFoundException e) {
                            Toast.makeText(getContext(),"There is no email client installed", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });
        }
        return vi;
    }

    public void getContacts()
    {
        // Check the SDK version and whether the permission is already granted or not.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(getContext(),Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
        }  else {
            Cursor cursor = contResv.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    Contact contact = new Contact();
                    contact.setIme(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    Cursor emails = contResv.query(Email.CONTENT_URI, null, Email.CONTACT_ID + " = " + id, null, null);
                    while (emails.moveToNext()) {
                        contact.setEmail(emails.getString(emails.getColumnIndex(Email.DATA)));
                        allContactsEmails.add(contact.getEmail());
                        break;
                    }
                    if(contact.getEmail() == null) contact.setEmail("");
                    emails.close();
                    allContacts.add(contact);
                } while (cursor.moveToNext());
            }

            cursor.close();
        }
    }

   @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getContacts();
            } else {
                Toast.makeText(getActivity(), "Until you grant the permission, we canot display the names", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
